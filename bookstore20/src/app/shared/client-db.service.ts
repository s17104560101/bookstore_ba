import { Injectable } from '@angular/core';
import Dexie from "dexie";
import {Book} from "./book";

@Injectable({
  providedIn: 'root'
})
export class ClientDbService extends Dexie{

  books:Dexie.Table<Book, string>;
  constructor() {
    super('book-db');
    this.version(1).stores({
        books: 'id, isbn, title, authors, published, user_id, subtitle, rating, images ,description'
    });

  }
}

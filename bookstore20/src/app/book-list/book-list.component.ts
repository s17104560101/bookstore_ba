import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Book, Image, Author} from "../shared/book" ;
import {BookStoreService} from "../shared/book-store.service";
import {ClientDbService} from "../shared/client-db.service";

@Component({
  selector: 'bs-book-list',
  templateUrl: './book-list.component.html',
  styles: []
})
export class BookListComponent implements OnInit {

    books : Book[];

    constructor(private bs: BookStoreService, private clientDbService:ClientDbService){
    }



    ngOnInit () {
        this.bs.getAll().subscribe(res => this.books = res);
        console.log("Here");
        //this.updateBooks();


    }

    async updateBooks(){
        this.books = await this.clientDbService.books.toArray();
    }

}

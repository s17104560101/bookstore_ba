import { Component } from '@angular/core' ;
import {Book} from './shared/book';
import {AuthentificationService} from "./shared/authentification.service";
@Component({
    selector : 'bs-root' ,
    templateUrl : './app.component.html'
})
export class AppComponent {


    constructor(private authService:AuthentificationService){

    }



    isLoggedIn(){
        return this.authService.isLoggedIn();
    }

    getLoginLabel(){
        if(this.isLoggedIn()){
            return "Logout";
        }else{
            return "Login"
        }


    }



    /*listOn = true;
    detailsOn=false;

    book:Book;

    showList(){
        this.listOn = true;
        this.detailsOn = false;
    }

    showDetails(book: Book){
        this.book = book;
        this.listOn = false;
        this.detailsOn = true;
}*/

    }

/*`<bs-book-list *ngIf="listOn" (showDetailsEvent)="showDetails($event)"></bs-book-list>
           <bs-book-details *ngIf="detailsOn" [book]="book" (showListEvent)="showList()" )></bs-book-details>`,*/

import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {AuthentificationService} from "../shared/authentification.service";
import {Router} from "@angular/router";


interface Response {
  response:string;
  result: {
    token:string;
  }

}

@Component({
  selector: 'bs-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;

  constructor(private fb:FormBuilder, private authService:AuthentificationService, private  router:Router) { }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
        username: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required]
    });

  }


  login(){
    const val = this.loginForm.value;
    if (val.username && val.password) {
      this.authService.login(val.username, val.password).subscribe(res=>{
        //cast on responsive interface
          const resObj = res as Response;
          if(resObj.response==="success"){
            this.authService.setLocalStoraged(resObj.result.token);
            this.router.navigateByUrl('/') //auf die startseite 'schmeißen ^^'
          }

      });
    }
  }

    isLoggedIn(){
    return this.authService.isLoggedIn();
  }

  logout(){
    this.authService.logout();
  }


}
